package com.models;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PlaceModel {
	
	
	
	private Integer id;
	private Double lat;
	private Double lon;
	private Integer Placeid ;
	private String PlaceName;
	private String description ;
	
	

	
	public String getplaceName() {
		return PlaceName;
	}

	public void setplaceName(String PlaceName) {
		this.PlaceName = PlaceName;
	}

	
	public void setDescription(String description) {
		this.description=description;
	}

	public String getDescription() {
		return description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getplaceid() {
		return Placeid;
	}

	public void setplaceid(Integer Placeid) {
		this.Placeid = Placeid;
	}
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	
	
	public static PlaceModel addnewplace(String PlaceName , String description , double lat , double lon)
	{
	try {
	Connection conn = DBConnection.getActiveConnection();
	String Sql = "Insert into Places (`placeName`, `description` ,`lat`, `long`) values (?,?,?,?)";

	PreparedStatement stmt ;
	stmt = conn.prepareStatement(Sql, Statement.RETURN_GENERATED_KEYS);
	stmt.setString(1, PlaceName);
	stmt.setString(2,description);
	stmt.setDouble(3, lat);
	stmt.setDouble(4, lon);

	stmt.executeUpdate();
				
	ResultSet rs = stmt.getGeneratedKeys();
	if (rs.next()) {
		PlaceModel user = new PlaceModel();
		user.Placeid = rs.getInt(1);
    	user.PlaceName=PlaceName;
     	user.description=description;
		user.lat=lat ;
     	user.lon=lon ;

	return user;
	}
	return null ;
	} 
	catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}


	public static boolean SavePlace( int plid, int uid)
	{
	try {
	Connection conn = DBConnection.getActiveConnection();
	String Sql = "insert into savedplaces ( `PID` , `UID`) values (?,?)";

	PreparedStatement stmt ;
	stmt = conn.prepareStatement(Sql, Statement.RETURN_GENERATED_KEYS);
	stmt.setInt(1,plid);
	stmt.setInt(2,uid);
	stmt.executeUpdate();
				
	return true;
	
	    } 
	catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false ;
		}
	
	public static ArrayList<String> SaveList(int userid) {
		try {
			ArrayList<Integer> IDS=new ArrayList<Integer>();
			ArrayList<String> places=new ArrayList<String>();
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Select `PID` from savedplaces where `UID` = ? ";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, userid);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) 
			{
				IDS.add(rs.getInt("PID"));
			}
			for(int i=0;i<IDS.size();i++)
			{
			String sql1 = "Select `placeName` from places where `placeId` = ?";
			PreparedStatement stmt1;
			stmt1 = conn.prepareStatement(sql1);
			stmt1.setInt(1, IDS.get(i));
			ResultSet rs1 = stmt1.executeQuery();
			if(rs1.next()) 
			{
				places.add(rs1.getString("placeName"));
			}
			}
			return places;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	
		
		
		
	}

	

}
