package com.models;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class CommentModel {
	
	
	private Integer id;

	private Integer checkinNum ;
	private String commentText ;
	
	
	
	
	

	public void setCommentText(String commentText) {
		this.commentText=commentText;
	}

	public String getCommentText() {
		return commentText;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	

	
	public void setcheckinNum(Integer checkinNum) {
		this.checkinNum = checkinNum;
	}

	public Integer getcheckinNum() {
		return checkinNum;
	}

	
	public static boolean Comment (int checkinNum , int id , String commentText)
	{
		String descName1 = null ;
		int userID1 = 0 ; 
		try {
	
		Connection conn = DBConnection.getActiveConnection();
		String sql = "insert into comments (`checknum` , `userID` , `text`) values (?,?,?) ";
                 
		PreparedStatement stmt;
		stmt = conn.prepareStatement(sql);
	
		stmt.setInt(1, checkinNum);
		stmt.setInt(2, id);
		stmt.setString(3, commentText);
		stmt.executeUpdate();
        stmt.clearBatch();
		
		sql = "select `name` from users where `id` = ? " ;
		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, id);
		ResultSet es = stmt.executeQuery();
		while (es.next()) 
		{
		   descName1= es.getString("name");
		}
	    
		stmt.clearBatch();
		
		sql = "select `id` from checks where `checkNum` = ? " ;
		stmt= conn.prepareStatement(sql);
			stmt.setInt(1, checkinNum);
			ResultSet res = stmt.executeQuery();
			while (res.next()) 
			{
			   userID1= res.getInt("id");
			}
		    
			stmt.clearBatch();
			
		sql =" insert into  notifications ( `checkNum` , `description` , `userId`) values (?,?,?) " ;
		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, checkinNum);
		stmt.setString(2, descName1 + " Commented on Your check-In ");
		stmt.setInt(3, userID1);
		
		stmt.executeUpdate();
		    
		return true;
	       } 
		catch (SQLException e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
		return false;
	}
	

}
