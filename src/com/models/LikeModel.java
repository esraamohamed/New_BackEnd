package com.models;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class LikeModel {

	
	
	
	
	private Integer id;
	
	private Integer Placeid ;


	private Integer checkinNum ;
	
	


	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getplaceid() {
		return Placeid;
	}

	public void setplaceid(Integer Placeid) {
		this.Placeid = Placeid;
	}
	
	

	public void setcheckinNum(Integer checkinNum) {
		this.checkinNum = checkinNum;
	}

	public Integer getcheckinNum() {
		return checkinNum;
	}
	
	
	
	public static boolean  Like (int checkinNum , int id)
	{
		String descName = null;
		int userID = 0 ;
		try {
	
		Connection conn = DBConnection.getActiveConnection();
		String sql = "insert into likes (`checknum` , `userID`) values (?,?) ";
                 
		PreparedStatement stmt;
		stmt = conn.prepareStatement(sql);
	
		stmt.setInt(1, checkinNum);
		stmt.setInt(2, id);
		stmt.executeUpdate();
		
		stmt.clearBatch();
		sql = "select `name` from users where `id` = ? " ;
		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, id);
		ResultSet es = stmt.executeQuery();
		while (es.next()) 
		{
		   descName= es.getString("name");
		}
	    
		stmt.clearBatch();
		
		sql = "select `id` from checks where `checkNum` = ? " ;
		stmt= conn.prepareStatement(sql);
			stmt.setInt(1, checkinNum);
			ResultSet res = stmt.executeQuery();
			while (res.next()) 
			{
			   userID= res.getInt("id");
			}
		    
			stmt.clearBatch();
			
		sql =" insert into  notifications ( `checkNum` , `description` , `userId`) values (?,?,?) " ;
		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, checkinNum);
		stmt.setString(2, descName + " Like Your check-In ");
		stmt.setInt(3, userID);
		
		stmt.executeUpdate();
		return true;
	       } 
		catch (SQLException e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
		return false;
	}
	
	
	
}

