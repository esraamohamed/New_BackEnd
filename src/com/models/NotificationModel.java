package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class NotificationModel {
	

	private Integer id;
	
	private String description ;


	

	public void setDescription(String description) {
		this.description=description;
	}

	public String getDescription() {
		return description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	

	public static ArrayList<String> NotificationList(int userId) {
		try {
			ArrayList<String> descriptions=new ArrayList<String>();
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Select `description` from notifications where `userId` = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, userId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) 
			{
				descriptions.add(rs.getString("description"));
			}
	
			return descriptions;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	

}
