package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class Check_InModel {
	
	

	private Integer id;
    private Integer Placeid ;
	private Integer checkinNum ;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getplaceid() {
		return Placeid;
	}

	public void setplaceid(Integer Placeid) {
		this.Placeid = Placeid;
	}
	

	public void setcheckinNum(Integer checkinNum) {
		this.checkinNum = checkinNum;
	}

	public Integer getcheckinNum() {
		return checkinNum;
	}

	
	

	public static Check_InModel CheckIn( int id , int Placeid)
	{
	try {
	Connection conn = DBConnection.getActiveConnection();
	String Sql = "insert into checks ( `id` , `pId`) values (?,?)";

	PreparedStatement stmt ;
	
	stmt = conn.prepareStatement(Sql, Statement.RETURN_GENERATED_KEYS);
	stmt.setInt(1,id);
	stmt.setInt(2,Placeid);
	stmt.executeUpdate();
				
	ResultSet re = stmt.getGeneratedKeys();
	if (re.next()) {
		Check_InModel Check = new Check_InModel();
		Check.checkinNum = re.getInt(1);
		Check.id=id;
        Check.Placeid=Placeid;
	return Check;
	}
	return null ;
	} 
	catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

	
	
	
	
	

}
