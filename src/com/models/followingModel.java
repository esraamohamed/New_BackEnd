package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class followingModel {

	private String name;
	private Integer id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static boolean follow(int id1, int id2) {
		try {

			Connection conn = DBConnection.getActiveConnection();
			String sql = "insert into followers (`id1`, `id2`) VALUES  (?,?)";

			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, id1);
			stmt.setInt(2, id2);
			stmt.executeUpdate();

			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static boolean Unfollow(int id1, int id2) {
		try {

			Connection conn = DBConnection.getActiveConnection();
			String sql = "delete from followers where  `id1`=? and`id2`=? ";

			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, id1);
			stmt.setInt(2, id2);
			stmt.executeUpdate();

			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static ArrayList<String> FollowList(int id2) {
		try {
			ArrayList<Integer> IDS = new ArrayList<Integer>();
			ArrayList<String> Following = new ArrayList<String>();
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Select `id1` from followers where `id2` = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id2);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				IDS.add(rs.getInt("id1"));
			}
			for (int i = 0; i < IDS.size(); i++) {
				String sql1 = "Select name from users where `id` = ?";
				PreparedStatement stmt1;
				stmt1 = conn.prepareStatement(sql1);
				stmt1.setInt(1, IDS.get(i));
				ResultSet rs1 = stmt1.executeQuery();
				if (rs1.next()) {
					Following.add(rs1.getString("name"));
				}
			}
			return Following;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
