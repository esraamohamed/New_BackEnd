package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;

import com.models.Check_InModel;
import com.models.CommentModel;
import com.models.DBConnection;
import com.models.LikeModel;
import com.models.NotificationModel;
import com.models.PlaceModel;
import com.models.UserModel;
import com.models.followingModel;

@Path("/f")

public class followingServices {

	@POST
	@Path("/follow")
	@Produces(MediaType.TEXT_PLAIN)
	public String follow(@FormParam("id1") int id1,
			@FormParam("id2") int id2)
	{
		//System.out.println("7aaaaaaaaamada");

		boolean user =  followingModel.follow(id1 , id2);
		JSONObject json = new JSONObject();
		json.put("done", user);
		return json.toJSONString();
	}
	
	@POST
	@Path("/Unfollow")
	@Produces(MediaType.TEXT_PLAIN)
	public String Unfollow(@FormParam("id1") int id1,
			@FormParam("id2") int id2)
	{
		boolean user =  followingModel.Unfollow(id1 , id2);
		JSONObject json = new JSONObject();
		json.put("done", user);
		return json.toJSONString();
	}
	

	
	
	

	@POST
	@Path("/GetFollowers")
	@Produces(MediaType.TEXT_PLAIN)
	public String GetFollowers(@FormParam("id2")  int id2 ){
		
		ArrayList<String> Following=new ArrayList<String>();
		JSONObject json = new JSONObject();
      Following=followingModel.FollowList(id2);
      for(int i=0;i<Following.size();i++)
      {
    	  json.put(i, Following.get(i));
      }
		return json.toJSONString();
	}
	
	
	
	
}
