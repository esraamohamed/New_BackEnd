package com.services;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;


import com.models.DBConnection;

import com.models.PlaceModel;




@Path("/places")
public class placeServices {

	@POST
	@Path("/NewPlace")
	@Produces(MediaType.TEXT_PLAIN)
	public String addNewPlace( @FormParam("placeName") String PlaceName ,
			@FormParam("description") String description,
			@FormParam("lat") Double lat,
			@FormParam("long") Double lon)
	{
		PlaceModel user = PlaceModel.addnewplace(PlaceName,description,lat ,lon);
		JSONObject json = new JSONObject();
		json.put("id", user.getplaceid());
		json.put("placeName", user.getplaceName());
		json.put("description", user.getDescription());
		json.put("lat", user.getLat());
		json.put("long", user.getLon());
		return json.toJSONString();
	}
	
	
	
	@POST
	@Path("/SavePlace")
	@Produces(MediaType.TEXT_PLAIN)
	public String save(@FormParam("PID") int plid,
			@FormParam("UID") int uid)
	{
		boolean user =  PlaceModel.SavePlace(plid , uid);
		JSONObject json = new JSONObject();
		json.put("done", user);
		return json.toJSONString();
	}
	
	@POST
	@Path("/GetSplaces")
	@Produces(MediaType.TEXT_PLAIN)
	public String Getsaved(@FormParam("UID")  int id ){
		
		ArrayList<String> places=new ArrayList<String>();
		 JSONObject json = new JSONObject();
          places=PlaceModel.SaveList(id);
      for(int i=0;i<places.size();i++)
      {
    	  json.put(i, places.get(i));
      }
		return json.toJSONString();
	}
	
	
	
	
}
