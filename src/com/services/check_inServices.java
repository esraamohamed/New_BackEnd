package com.services;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;

import com.models.Check_InModel;
import com.models.CommentModel;
import com.models.DBConnection;
import com.models.LikeModel;
import com.models.NotificationModel;
import com.models.PlaceModel;
import com.models.UserModel;
import com.models.followingModel;

@Path("/ch")

public class check_inServices {
	

	@POST
	@Path("/CheckIn")
	@Produces(MediaType.TEXT_PLAIN)
	public String CheckIn( @FormParam("id") int id ,
			@FormParam("pId") int Placeid)
			
	{
		Check_InModel CheckIn = Check_InModel.CheckIn(id, Placeid);
		JSONObject json = new JSONObject();
		json.put("checkinNum", CheckIn.getcheckinNum());
		json.put("id", CheckIn.getId());
		json.put("pId", CheckIn.getplaceid());
		return json.toJSONString();
	}
	
	
	
	
	

}
