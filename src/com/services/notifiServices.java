package com.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;

import com.models.Check_InModel;
import com.models.CommentModel;
import com.models.DBConnection;
import com.models.LikeModel;
import com.models.NotificationModel;
import com.models.PlaceModel;
import com.models.UserModel;
import com.models.followingModel;

@Path("/Notifi")

public class notifiServices {

	
	@POST
	@Path("/GetAllNotif")
	@Produces(MediaType.TEXT_PLAIN)
	public String NotificationList(@FormParam("userId")  int userId ){
		
		ArrayList<String> descriptions=new ArrayList<String>();
		JSONObject json = new JSONObject();
      descriptions=NotificationModel.NotificationList(userId);
      for(int i=0;i<descriptions.size();i++)
      {
    	  json.put(i, descriptions.get(i));
      }
		return json.toJSONString();
	}
	
	
	
}
